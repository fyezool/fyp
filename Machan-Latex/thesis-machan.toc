
\contentsline {chapter}{\hspace {\toc@tab@a }\MakeUppercase {ACKNOWLEDGEMENT}}{i}{section*.2}
\contentsline {chapter}{\hspace {\toc@tab@a }\MakeUppercase {ABSTRACT}}{ii}{section*.4}
\contentsline {chapter}{\hspace {\toc@tab@a }\MakeUppercase {TABLE OF CONTENTS}}{iii}{section*.6}
\contentsline {chapter}{\hspace {\toc@tab@a }\MakeUppercase {LIST OF TABLES}}{vi}{section*.8}
\contentsline {chapter}{\hspace {\toc@tab@a }\MakeUppercase {LIST OF FIGURES}}{vii}{section*.10}
\contentsline {chapter}{\hspace {\toc@tab@a }\MakeUppercase {LIST OF ABBREVIATIONS}}{ix}{section*.12}
\addvspace {\@twolinespacing }
\contentsline {chapter}{\numberline {\MakeUppercase {Chapter} 1}\MakeUppercase {Introduction}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Problem Statement}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Objective}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Project Scope}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Chapter Summary}{3}{section.1.5}
\addvspace {\@twolinespacing }
\contentsline {chapter}{\numberline {\MakeUppercase {Chapter} 2}\MakeUppercase {Literature Review}}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Service Learning}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Rural Tourism}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Mobile application development}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Studies of Native Mobile Development}{9}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Android mobile platform}{10}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}iOS mobile platform}{11}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}HTML5 Hybrid Mobile Framework Development}{12}{section.2.5}
\contentsline {section}{\numberline {2.6}Cross-Platform Mobile Development}{13}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Xamarin by Microsoft}{13}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Flutter by Google}{14}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}React Native by Facebook}{15}{subsection.2.6.3}
\contentsline {section}{\numberline {2.7}Web Application development}{16}{section.2.7}
\contentsline {section}{\numberline {2.8}Software Development Models}{17}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Waterfall methodology}{18}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Spiral model}{20}{subsection.2.8.2}
\contentsline {subsection}{\numberline {2.8.3}Incremental Model}{22}{subsection.2.8.3}
\contentsline {subsection}{\numberline {2.8.4}V-Shaped model}{24}{subsection.2.8.4}
\contentsline {subsection}{\numberline {2.8.5}Scrum}{25}{subsection.2.8.5}
\contentsline {section}{\numberline {2.9}Architecture of Pekan Machan Tourism mobile Application}{26}{section.2.9}
\contentsline {section}{\numberline {2.10}Proposed design}{27}{section.2.10}
\contentsline {subsection}{\numberline {2.10.1}Login screen}{27}{subsection.2.10.1}
\contentsline {subsection}{\numberline {2.10.2}Main navigation}{28}{subsection.2.10.2}
\contentsline {subsection}{\numberline {2.10.3}Maps}{29}{subsection.2.10.3}
\contentsline {section}{\numberline {2.11}Hardware Requirements}{30}{section.2.11}
\contentsline {subsection}{\numberline {2.11.1}Smartphone for prototyping}{30}{subsection.2.11.1}
\contentsline {subsection}{\numberline {2.11.2}Personal computer for development}{31}{subsection.2.11.2}
\contentsline {section}{\numberline {2.12}Studies of existing systems}{32}{section.2.12}
\contentsline {subsection}{\numberline {2.12.1}Visit Korea}{32}{subsection.2.12.1}
\contentsline {subsection}{\numberline {2.12.2}Kerala Tourism}{33}{subsection.2.12.2}
\contentsline {subsection}{\numberline {2.12.3}Tanzania Tourism}{34}{subsection.2.12.3}
\contentsline {section}{\numberline {2.13}Software Requirements}{35}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}IDE}{35}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}Image Manipulation program}{36}{subsection.2.13.2}
\contentsline {subsection}{\numberline {2.13.3}Operating System}{36}{subsection.2.13.3}
\contentsline {subsection}{\numberline {2.13.4}Flutter SDK}{37}{subsection.2.13.4}
\contentsline {section}{\numberline {2.14}Chapter Summary}{38}{section.2.14}
\addvspace {\@twolinespacing }
\contentsline {chapter}{\numberline {\MakeUppercase {Chapter} 3}\MakeUppercase {Methodology}}{39}{chapter.3}
\contentsline {section}{\numberline {3.1}DevOps}{40}{section.3.1}
\contentsline {section}{\numberline {3.2}DevOps lifecycle}{40}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Plan and measure}{41}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Develop and test}{42}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Release and deploy}{45}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Monitor and optimize}{47}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Chapter Summary}{47}{section.3.3}
\addvspace {\@twolinespacing }
\contentsline {chapter}{\numberline {\MakeUppercase {Chapter} 4}\MakeUppercase {Result and Discussions}}{48}{chapter.4}
\contentsline {section}{\numberline {4.1}Project implementation}{48}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Application Evaluation using Shneiderman 8 Golden rule}{52}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Chapter Summary}{54}{section.4.2}
\addvspace {\@twolinespacing }
\contentsline {chapter}{\numberline {\MakeUppercase {Chapter} 5}\MakeUppercase {Conclusion}}{55}{chapter.5}
\contentsline {section}{\numberline {5.1}Project limitation}{55}{section.5.1}
\contentsline {section}{\numberline {5.2}Future improvements}{55}{section.5.2}
\contentsline {section}{\numberline {5.3}Chapter Summary}{56}{section.5.3}
\addvspace {\@twolinespacing }
\contentsline {chapter}{\MakeUppercase {REFERENCES}}{57}{section*.54}
